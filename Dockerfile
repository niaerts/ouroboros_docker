FROM debian:jessie

RUN apt update && apt install -y git protobuf-c-compiler cmake libgcrypt20-dev libssl-dev libfuse-dev dnsutils swig cmake-curses-gui gdb rsyslog && rm -rf /var/lib/apt/lists/*
ADD ./ouroboros /tmp/ouroboros_src
RUN cd /tmp/ouroboros_src && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=DebugLSan -DCONNECT_TIMEOUT=120000 -DQUERY_TIMEOUT=120000 -DREG_TIMEOUT=120000 -DSHM_BUFFER_SIZE=1024 -DIPCP_FLOW_STATS=True .. && \
    make install

WORKDIR "/root"
CMD irmd --stdout
